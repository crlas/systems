(use-modules
 (gnu)
 (nongnu packages linux)
 (nongnu system linux-initrd))

(use-service-modules cups desktop networking ssh xorg virtualization nix)
(use-package-modules shells ncurses python curl version-control wm glib linux)

(operating-system
 (locale "en_CA.utf8")
 (timezone "America/Toronto")
 (keyboard-layout (keyboard-layout "us" "mac"))
 (host-name "ouroboros")
 
 (kernel linux-lts)
 (initrd microcode-initrd)
 
 (kernel-arguments
  '("modprobe.blacklist=b43,b43legacy,ssb,bcm43xx,brcm80211,brcmfmac,brcmsmac,bcma"))
 (kernel-loadable-modules (list broadcom-sta))
 
 (firmware (cons* broadcom-bt-firmware
                  %base-firmware))

 (users (cons* (user-account
                (name "crlas")
                (comment "Chatman R. Las")
                (group "users")
                (home-directory "/home/crlas")
		(shell (file-append fish "/bin/fish"))
                (supplementary-groups
		 '("wheel" "netdev" "kvm" "tty" "input" "lp" "audio" "video" "libvirt")))
               %base-user-accounts))

 (groups %base-groups)

 (name-service-switch %mdns-host-lookup-nss)

 (packages (cons*
	    ;; Essentials
	    (specification->package "fish")
	    (specification->package "python")
	    (specification->package "ncurses")
	    (specification->package "wget")
	    (specification->package "curl")
	    (specification->package "git")
	    (specification->package "nss-certs")
	    (specification->package "nix")

	    ;; Emacs integration
            (specification->package "emacs")
	    (specification->package "emacs-guix")

	    ;; Shell addons
	    (specification->package "stow")
	    (specification->package "diff-so-fancy")
	    (specification->package "tree")
	    (specification->package "gnupg")
	    (specification->package "bpytop")

	    ;; File format support
	    (specification->package "exfat-utils")
	    (specification->package "fuse-exfat")
	    (specification->package "gvfs")
	    (specification->package "ntfs-3g")
	    (specification->package "udiskie")
	    (specification->package "unzip")
	    (specification->package "p7zip")

	    ;; File sharing
	    (specification->package "syncthing")

	    ;; Virtualization & Containerization
	    (specification->package "ovmf")
	    (specification->package "distrobox")
	    
	    ;; Desktop
	    (specification->package "emacs-exwm")
	    (specification->package "emacs-desktop-environment")

	    ;; Application protocols
	    (specification->package "qtwayland")
	    (specification->package "egl-wayland")
	    (specification->package "gsettings-desktop-schemas")
	    
	    ;; Fonts
	    (specification->package "fontconfig")
	    (specification->package "fontmanager")
	    (specification->package "font-awesome")
	    (specification->package "font-microsoft-cascadia")
	    (specification->package "font-hack")

	    ;; Icons
	    (specification->package "adwaita-icon-theme")

	    ;; Cursors
	    (specification->package "bibata-cursor-theme")

	    ;; Themes
	    (specification->package "gnome-themes-extra")
	    
	    ;; Desktop settings
	    (list glib "bin")
	    (specification->package "glibc-locales")
	    (specification->package "gnome-settings-daemon")
	    (specification->package "gnome-tweaks")
	    (specification->package "lxappearance")
	    (specification->package "qt5ct")
	    (specification->package "dconf")
	    (specification->package "dconf-editor")
	    (specification->package "xinput")
	    (specification->package "xrandr")
	    (specification->package "xmodmap")

	    ;; File manager addons
	    (specification->package "ffmpegthumbnailer")
	    (specification->package "libappindicator")

	    ;; Display settings
	    (specification->package "redshift")

	    ;; Notifications
	    (specification->package "dunst")

	    ;; Audio
	    (specification->package "pipewire")
	    (specification->package "wireplumber")
	    (specification->package "mpd")
	    (specification->package "mpd-mpc")

	    ;; Terminal
            (specification->package "kitty")
	    (specification->package "xclip")
	    
	    %base-packages))
 
 (services
  (cons*
   (service openssh-service-type)
   (service libvirt-service-type)
   (service virtlog-service-type)
   (service qemu-binfmt-service-type
	    (qemu-binfmt-configuration
	     (platforms (lookup-qemu-platforms "arm" "aarch64"))))
   (service nix-service-type)
   (modify-services %desktop-services
		    (gdm-service-type config =>
				      (gdm-configuration
				       (inherit config)
				       (wayland? #t))))))
 
 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              (keyboard-layout keyboard-layout)))
 
 (swap-devices (list (swap-space
                      (target (uuid
                               "6ff1e351-d527-43c3-b5f5-487e890163f2")))))
 
 (file-systems (cons* (file-system
                       (mount-point "/boot/efi")
                       (device (uuid "FA19-4C90" 'fat32))
                       (type "vfat"))
                      (file-system
                       (mount-point "/")
                       (device (uuid
                                "8fb2131a-0b52-49f5-97d9-72a6877fe3fb" 'ext4))
                       (type "ext4"))
                      (file-system
                       (mount-point "/home")
                       (device (uuid
                                "efe7f893-09aa-4983-b4b7-5448a97e4ef1" 'ext4))
                       (type "ext4"))
		      %base-file-systems)))
